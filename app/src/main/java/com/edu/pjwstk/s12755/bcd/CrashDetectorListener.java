package com.edu.pjwstk.s12755.bcd;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;

public class CrashDetectorListener implements SensorEventListener {
    private static final int FORCE_THRESHOLD = 10000;
    private static final int TIME_THRESHOLD = 75;
    private static final int SHAKE_TIMEOUT = 500;
    private static final int SHAKE_DURATION = 150;
    private static final int SHAKE_COUNT = 1;
    private final String fileName;

    private SensorManager mSensorMgr;
    private float lastX = -1.0f;
    private float lastY = -1.0f;
    private float lastZ = -1.0f;
    private long mLastTime;
    private crashedDetectedListener crashedDetectedListener;
    private Context context;
    private int shakeCount = 0;
    private long lastShake;
    private long lastForce;

    private StringBuffer logs;
    private boolean crashDetected;

    public CrashDetectorListener(Context context) {
        this.context = context;
        logs = new StringBuffer();
        fileName = "LOGS_BCD_" + new Date(System.currentTimeMillis()).toString();
        writeFileOnInternalStorage(context, fileName, "TIME;SPEED;SOMETHING\n");
        resume();

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            long now = System.currentTimeMillis();

            if ((now - lastForce) > SHAKE_TIMEOUT) {
                shakeCount = 0;
            }

            if ((now - mLastTime) > TIME_THRESHOLD) {
                long diff = now - mLastTime;

                float speed = Math.abs(x + y + z - lastX - lastY - lastZ) / diff * 10000;
                Log.d("SensorChanged:",new Date(System.currentTimeMillis()) + "SPEED:" + speed + " --- SOMETHING: " + Math.abs(x + y + z - lastX - lastY - lastZ) + " -- CURRENT XYZ: " + Math.abs(x + y + z));
                logs.append(new Date(System.currentTimeMillis()) + ";" + speed + ";" + Math.abs(x + y + z - lastX - lastY - lastZ) + "\n");

                if (speed > FORCE_THRESHOLD) {
                    if ((++shakeCount >= SHAKE_COUNT) && (now - lastShake > SHAKE_DURATION)) {
                        lastShake = now;
                        shakeCount = 0;
                        if (crashedDetectedListener != null) {
                            writeFileOnInternalStorage(context, fileName, logs.toString());
                            crashedDetectedListener.crashDetected();

                        }
                    }
                    lastForce = now;
                }
                mLastTime = now;
                lastX = x;
                lastY = y;
                lastZ = z;
            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setCrashedDetectedListener(crashedDetectedListener listener) {
        crashedDetectedListener = listener;
    }

    public void resume() {
        mSensorMgr = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if (mSensorMgr == null) {
            throw new UnsupportedOperationException("Sensors not supported");
        }
        boolean supported = mSensorMgr.registerListener(this, mSensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
        if (!supported) {
            mSensorMgr.unregisterListener(this, mSensorMgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
            throw new UnsupportedOperationException("Accelerometer not supported");
        }
    }

    public void writeFileOnInternalStorage(Context mcoContext, String sFileName, String sBody) {
        File file = new File(mcoContext.getFilesDir(), "mydir");
        if (!file.exists()) {
            file.mkdir();
        }

        try {
            File gpxfile = new File(file, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public interface crashedDetectedListener {
        void crashDetected();
    }

}