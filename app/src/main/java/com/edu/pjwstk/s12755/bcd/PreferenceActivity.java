package com.edu.pjwstk.s12755.bcd;


import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Struct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PreferenceActivity extends AppCompatActivity {
    public static String emergencyNumber;
    public EditText saveNumberButton;
    public SharedPreferences settings;
    public SharedPreferences.Editor editor;
    public EditText editTextMessage;
    public EditText editTextNumber;
    public Spinner spinnerSendingDelay;
    public Spinner spinnerAlertSound;

    public Spinner spinnerTestContact;

    private Contact emergencyContact;

    @Override
    public void onBackPressed() {
        savePreferences();
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.drawable.array);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        settings = getSharedPreferences("BCDpreferences", Activity.MODE_PRIVATE);
        editor = settings.edit();

        TextView textView1 = (TextView) findViewById(R.id.textView_pref_alert_contact);
        TextView textView2 = (TextView) findViewById(R.id.textView_pref_message);
        TextView textView3 = (TextView) findViewById(R.id.textView_pref_alert_sound);
        TextView textView4 = (TextView) findViewById(R.id.textView_pref_sending_delay);
        textView1.setTypeface(Typeface.createFromAsset(getAssets(), "Tw Cen MT.ttf"));
        textView2.setTypeface(Typeface.createFromAsset(getAssets(), "Tw Cen MT.ttf"));
        textView3.setTypeface(Typeface.createFromAsset(getAssets(), "Tw Cen MT.ttf"));
        textView4.setTypeface(Typeface.createFromAsset(getAssets(), "Tw Cen MT.ttf"));

        final TextView textViewNumber = (TextView) findViewById(R.id.textView_pref_alert_number);


        editTextMessage = (EditText) findViewById(R.id.editText_pref_message);
        editTextMessage.setText(settings.getString("message", ""));
        textViewNumber.setText(settings.getString("emergency_number", ""));

        addItemsOnSpinnerSendingDelay();
        addItemsOnSpinnerAlertSound();
        spinnerSendingDelay.setSelection(settings.getInt("sending_delay",0));
        spinnerAlertSound.setSelection(settings.getInt("alert_sound", 0));

        spinnerTestContact = (Spinner) findViewById(R.id.spinner_test_contact);

        if(verifyPermissions()){
            getNumber(this.getContentResolver());
            if(settings.getInt("id_of_contact",0) != 0){
                for (int x = 0; x<ListForAdapter.size(); x++) {
                    if(ListForAdapter.get(x).equals(spinnerMap.get(settings.getInt("id_of_contact",0)).getName()) && spinnerMap.containsKey(settings.getInt("id_of_contact",0)) ){
                        spinnerTestContact.setSelection(x);
                        emergencyContact = spinnerMap.get(settings.getInt("id_of_contact", 0));
                    }
                }
            }
            spinnerTestContact.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    if(spinnerMap.get(mapPositionOnSpinnerAndID.get(i)) != null){
                        emergencyContact = spinnerMap.get(mapPositionOnSpinnerAndID.get(i));
                        textViewNumber.setText(emergencyContact.getNumber());
                        Toast.makeText(PreferenceActivity.this,"Contact " + emergencyContact.getName() + " have number " + emergencyContact.getNumber(),Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }


        }


        @Override
        public boolean onCreateOptionsMenu (Menu menu){
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected (MenuItem item){
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.

            savePreferences();

            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_about) {
                startActivity(new Intent(this,AboutActivity.class));
            }


            return super.onOptionsItemSelected(item);
        }



        public void addItemsOnSpinnerSendingDelay(){
        spinnerSendingDelay = (Spinner) findViewById(R.id.spinner_pref_sending_delay);
        List<String> listOfTimes = new ArrayList<>();
        listOfTimes.add("30s");
        listOfTimes.add("60s");
        listOfTimes.add("90s");
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOfTimes);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerSendingDelay.setAdapter(arrayAdapter);
        }

    public void addItemsOnSpinnerAlertSound(){
        spinnerAlertSound = (Spinner) findViewById(R.id.spinner_pref_alert_sound);
        List<String> listOfAlertSound = new ArrayList<>();
        listOfAlertSound.add("ALERT");
        listOfAlertSound.add("NUCLEAR");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listOfAlertSound);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAlertSound.setAdapter(arrayAdapter);
    }
    private void loadPrecferences () {

    }
        private void savePreferences(){
//            if (editTextNumber.getText() != null)
//                try {
//                    emergencyNumber = editTextNumber.getText().toString();
//                    editor.putString("emergency_number", emergencyNumber);
//                    editor.commit();
//                    Toast.makeText(getApplicationContext(),
//                            "The emergency contact numbers have been saved.",
//                            Toast.LENGTH_SHORT).show();
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(), e.getMessage(),
//                            Toast.LENGTH_SHORT).show();
//                }

            editor.putString("message", editTextMessage.getText().toString());
            editor.commit();

            editor.putInt("sending_delay", spinnerSendingDelay.getSelectedItemPosition());
            editor.commit();

            int selectedAlertSound = spinnerAlertSound.getSelectedItemPosition();
            editor.putInt("alert_sound", selectedAlertSound);
            editor.commit();

            int selectedContactNumberPosition = emergencyContact.getId();
            editor.putInt("id_of_contact", selectedContactNumberPosition);
            editor.putString("emergency_number",emergencyContact.getNumber());
            editor.commit();
        }

//    ListView lv;
    HashMap<Integer,Contact> spinnerMap = new HashMap<Integer, Contact>();
    List<String> ListForAdapter = new ArrayList<>();
    HashMap<Integer,Integer> mapPositionOnSpinnerAndID = new HashMap<Integer, Integer>();
    public void getNumber(ContentResolver cr)
    {
        String phoneNumber;
        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        int x = 0;
        while (phones.moveToNext())
        {
            int id=phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            System.out.println(id + " "+ name + " " + phoneNumber );
            ListForAdapter.add(name);
            spinnerMap.put(id,new Contact(id,name,phoneNumber));
            mapPositionOnSpinnerAndID.put(x,id);
            x++;
        }
        phones.close();// close cursor
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_layout,ListForAdapter);
        adapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerTestContact.setAdapter(adapter);
        //display contact numbers in the list
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean verifyPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this,Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED) {

            String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.SEND_SMS, Manifest.permission.WAKE_LOCK, Manifest.permission.READ_CONTACTS};

            requestPermissions(perm,1337);
            return false;
        }else{
            return  true;
        }
    }
    }
