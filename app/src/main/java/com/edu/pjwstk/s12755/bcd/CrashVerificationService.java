package com.edu.pjwstk.s12755.bcd;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

public class CrashVerificationService extends Service {
    private double latitude;
    private double longitude;
    private double lastLatitude;
    private double lastLongitude;
    public float[] distance;
    private boolean accidentLocationHappened = true;
    private LocationListener locationListener;
    private PowerManager.WakeLock wakeLock;
    private Handler handler;

    public CrashVerificationService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        distance = new float[1];
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();


                if (accidentLocationHappened == false) {

                    Location.distanceBetween(lastLatitude, lastLongitude, latitude, longitude, distance);

                    Toast.makeText(getApplicationContext(), "LOCATION DISTANCE: " + distance[0], Toast.LENGTH_LONG).show();

                }
                if (accidentLocationHappened) {
                    lastLatitude = latitude;
                    lastLongitude = longitude;
                    accidentLocationHappened = false;
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        PowerManager mgr = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "CrashVerificationWakeLock");
        wakeLock.acquire();

        final LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        updateLocation(locationManager);


        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateLocation(locationManager);
            }
        }, 30000);

        handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    if (distance[0] > 30) {
                                        startService((new Intent(getApplicationContext(), CrashDetector.class)));
                                    } else {
                                        startActivity(new Intent(getApplicationContext(), Sender.class));
                                    }
                                    stopSelf();
                                }
                            }
                , 60000);

    }

    private void updateLocation(LocationManager locationManager) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, locationListener);
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        wakeLock.release();
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }


}
