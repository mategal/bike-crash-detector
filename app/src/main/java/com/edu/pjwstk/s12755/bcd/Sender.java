package com.edu.pjwstk.s12755.bcd;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


public class Sender extends Activity implements LocationListener {
    private LocationManager locationManager;
    private double latitude, longitude;
    double lastLatitude, lastLongitude;
    private String emergency_number;
    private String message;
    private int sendingDelay;
    private java.util.ArrayList<String> partsOfMessage;
    private SharedPreferences settings;
    private TextView textViewCounter;
    private CountDownTimer countDownTimer;
    private MediaPlayer mediaPlayerAlert;
    private boolean accidentLocationHappened = true;
    private float[] distance;
    private int timeForCheckingLocaltion;

    @RequiresApi(api = Build.VERSION_CODES.M)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_under_alert);
        this.setFinishOnTouchOutside(false);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        stopService(new Intent(this, CrashDetector.class));

        textViewCounter = (TextView) findViewById(R.id.textViewCounter);
        distance = new float[1];
        settings = getSharedPreferences("BCDpreferences", Activity.MODE_PRIVATE);
        emergency_number = settings.getString("emergency_number","");
        message =settings.getString("message", "I had incident. Try to call me or call to ER. There is my position:");
        sendingDelay = (settings.getInt("sending_delay",0)+1)*30000;
        loadAlertSound();

        final SmsManager sms = SmsManager.getDefault();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.SEND_SMS};
            requestPermissions(perm,1337);
            return;
        }

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 10, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, this);

//        final Handler checking = new Handler();
//        timeForCheckingLocaltion = 60000;
//        checking.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if(accidentLocationHappened){
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        message += " http://maps.google.com/?q="+String.valueOf(latitude)+","+String.valueOf(longitude);
                        partsOfMessage = sms.divideMessage(message);
                        sms.sendMultipartTextMessage(emergency_number,null, partsOfMessage,null,null);
                        restartAppToBaseState();
                    }
                }, sendingDelay);
                counterSetter();
                countDownTimer.start();
//                }else{
//                    stopService(new Intent(getApplicationContext(), CrashDetector.class));
//                    startService(new Intent(getApplicationContext(), CrashDetector.class));
//                }


//            }
//        }, timeForCheckingLocaltion);

        Button dismiss = (Button) findViewById(R.id.dismissB);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(1);
            }
        });


    }

    private void restartAppToBaseState() {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }


    @Override
    public void onLocationChanged(Location location){
        latitude=location.getLatitude();
        longitude=location.getLongitude();

//        Location.distanceBetween(lastLatitude,lastLongitude,latitude,longitude,distance);
//        if(accidentLocationHappened == false ) {
//            Toast.makeText(getApplicationContext(),"LOCATION DISTANCE: " +distance[0] ,Toast.LENGTH_LONG).show();
//            if(distance[0] > 50){
//                accidentLocationHappened = true;
//            }
//        }
//        if(accidentLocationHappened){
//        lastLatitude = latitude;
//        lastLongitude = longitude;
//        accidentLocationHappened = false;
//        }

//        Toast.makeText(getApplicationContext(),"Lat" + latitude + " and Long " + longitude + " extracted", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onProviderDisabled(String provider){
    }
    @Override
    public void onProviderEnabled(String provider){
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    private void counterSetter(){
        countDownTimer = new CountDownTimer(sendingDelay, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                textViewCounter.setText(getString(R.string.message_countdown_part1) +" "+ (millisUntilFinished/1000) +" " + getString(R.string.message_countdown_part2));
                mediaPlayerAlert.start();
            }

            @Override
            public void onFinish() {

            }

        };
    }

    private void loadAlertSound(){
        HashMap<Integer, Integer> hashMapSounds = new HashMap<Integer,Integer>();

        hashMapSounds.put(0,R.raw.alarm);
        hashMapSounds.put(1,R.raw.nuclear);

        mediaPlayerAlert = MediaPlayer.create(this, hashMapSounds.get(settings.getInt("alert_sound",0)));
    }
}