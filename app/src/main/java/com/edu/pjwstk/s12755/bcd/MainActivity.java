package com.edu.pjwstk.s12755.bcd;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static boolean flag;
    PowerManager.WakeLock wakeLock;
    Animation buttonAnimatio;
    public SharedPreferences settings;
    public SharedPreferences.Editor editor;

    //test
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundResource(R.drawable.array);
        setSupportActionBar(toolbar);


        verifyPermissions();

        final SharedPreferences sharedPreferences = getSharedPreferences("BCDpreferences", AppCompatActivity.MODE_PRIVATE);

        final Button buttonForCrashDetectionOnOff=(Button)findViewById(R.id.button_activation);
        final ImageView greenOutsideBikeButton = (ImageView)findViewById(R.id.image_outside_bike_ico);
        final TextView textView_OnOff = (TextView)findViewById(R.id.textView_onOff);
        textView_OnOff.setTypeface(Typeface.createFromAsset(getAssets(), "Tw Cen MT.ttf"));

        buttonAnimatio =new RotateAnimation(0, 359, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        buttonAnimatio.setDuration(2000);
        buttonAnimatio.setInterpolator(new LinearInterpolator());
        buttonAnimatio.setRepeatCount(Animation.INFINITE);

        settings = getSharedPreferences("BCDpreferences", Activity.MODE_PRIVATE);
        editor = settings.edit();


        flag=true;

        greenOutsideBikeButton.setVisibility(View.INVISIBLE);

                buttonForCrashDetectionOnOff.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (flag) {
                            if(verifyPermissions()){

                                if(sharedPreferences.getString("emergency_number","") == ""){
                                    Toast.makeText(MainActivity.this, "Set emergency number.", Toast.LENGTH_LONG).show();
                                    Intent preferenceIntent = new Intent(MainActivity.this,PreferenceActivity.class);
                                    startActivity(preferenceIntent);
                                }else{

                                    Toast.makeText(MainActivity.this, "ACTIVATED!", Toast.LENGTH_SHORT).show();
                                    buttonForCrashDetectionOnOff.setBackgroundResource(R.drawable.inside_bicycle_ico);
                                    greenOutsideBikeButton.setVisibility(View.VISIBLE);
                                    greenOutsideBikeButton.getRotation();
                                    startService(new Intent(getApplicationContext(), CrashDetector.class));

                                    textView_OnOff.setText("ON");
                                    textView_OnOff.setTextColor(Color.GREEN);
                                    flag = false;
                                    greenOutsideBikeButton.startAnimation(buttonAnimatio);
                                    toolbar.setVisibility(View.INVISIBLE);
                                }
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "DEACTIVATED!", Toast.LENGTH_SHORT).show();
                            buttonForCrashDetectionOnOff.setBackgroundResource(R.drawable.red_button);
                            greenOutsideBikeButton.clearAnimation();
                            greenOutsideBikeButton.setVisibility(View.INVISIBLE);


                            toolbar.setVisibility(View.VISIBLE);

                            textView_OnOff.setText("OFF");
                            textView_OnOff.setTextColor(Color.RED);
                            stopService(new Intent(getApplicationContext(), CrashDetector.class));
                            stopService(new Intent(getApplicationContext(), CrashVerificationService.class));
                            flag = true;
                        }

                    }
                });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            startActivity(new Intent(this,AboutActivity.class));
        }
        if (id == R.id.action_settings){
            if(verifyPermissions()){
                startActivity(new Intent(this, PreferenceActivity.class));
            }
        }

        return super.onOptionsItemSelected(item);
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean verifyPermissions(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this,Manifest.permission.WAKE_LOCK) != PackageManager.PERMISSION_GRANTED) {

            String[] perm = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.SEND_SMS, Manifest.permission.WAKE_LOCK, Manifest.permission.READ_CONTACTS};

            requestPermissions(perm,1337);
            return false;
        }else{
            return true;
        }
    }
}
