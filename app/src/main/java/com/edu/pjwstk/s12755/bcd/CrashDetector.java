package com.edu.pjwstk.s12755.bcd;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.EventListener;


public class CrashDetector extends Service implements CrashDetectorListener.crashedDetectedListener {
    private CrashDetectorListener crashDetectorListener;
    private SensorManager mSensorManager;
    private PowerManager.WakeLock wakeLock;
    private LocationManager noticedAccidentLocation;
    private LocationManager veryficatorAccidentLocation;
    public int check;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {

        super.onCreate();
        this.mSensorManager = ((SensorManager) getSystemService(Context.SENSOR_SERVICE));
        PowerManager mgr = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        wakeLock.acquire();

        crashDetectorListener = new CrashDetectorListener(this);
        crashDetectorListener.setCrashedDetectedListener(this);
        mSensorManager.registerListener(crashDetectorListener,null,SensorManager.SENSOR_DELAY_GAME);
//        Toast.makeText(CrashDetector.this, "Service is created!", Toast.LENGTH_LONG).show();
        Log.d(getPackageName(), "Created the Service!");
        check = 1;
    }

    @Override
    public void crashDetected() {
        if (check == 1) {
            Toast.makeText(CrashDetector.this, "Crashed!", Toast.LENGTH_LONG).show();
            final Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vib.vibrate(200);
//            Intent i = new Intent();
//            i.setClass(this, Sender.class);
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(i);
            startService(new Intent(getApplicationContext(),CrashVerificationService.class));
            stopSelf();
        }

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);

    }
    public void onDestroy(){
        wakeLock.release();
        mSensorManager.unregisterListener(crashDetectorListener);
        super.onDestroy();
        check=0;
        Log.d(getPackageName(),"Service Destroyed.");
    }
}
